<?php

namespace Drupal\contentu_express\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for content type synchronisation.
 *
 * @internal
 */
class SynchronisationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'contentu_express.synchronisation',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contentu_express_synchronisation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('contentu_express.synchronisation');

    $types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();

    foreach ($types as $type) {
      $content_types[$type->id()] = $type->label();
    }

    // Need help setting this up so that the options are content types.
    $form['all_content_types'] = [
      '#type' => 'checkboxes',
      '#title' => 'Content Type',
      '#description' => 'Choose the Content Type for Syncing',
      '#options' => $content_types,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('contentu_express.synchronisation')
      ->set('node', $form_state->getValue('all_content_types'))
      ->save();
  }

}
